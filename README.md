# DFS Construct

DFS Construct is a set of bash scripts to automate a [multilib build from scratch](https://linuxfromscratch.org). Adding **dpkg** and **apt** from [Debian](https://debian.org), but also built from scratch as well as the packages we add.



## Features


## Documentation
Some documentation might be found in [the project's wiki](../../wikis/home). There may also be some on the [homepage](https://dfs-linux.org).

## License
DFS Construct is GPLv3 Licensed. 

    DFS Construct
    Copyright (C) 2021 JMColeman <hyperclock@dfs-linux.org>, DFS Linux

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

See [LICENSE](LICENSE) for details.


## Support
Bugs, issues, questions, suggestions and similar can (and should) be placed into [the projects issues area](../../issues). DFS Linux also maintains a community [FORUM](https://dfs-linux.org/forums)
